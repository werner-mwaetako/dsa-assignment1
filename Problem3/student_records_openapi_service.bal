import ballerina/http;

listener http:Listener ep0 = new (4040, config = {host: "localhost"});

service / on ep0 {
    resource function get students() returns typedesc<Student> {
        return Student;
    }
    resource function put students(@http:Payload CourseDetails payload) returns typedesc<CourseDetails> {
        return CourseDetails;
    }
    resource function post students(@http:Payload Student payload) returns typedesc<CreatedInlineResponse201> {
        return CreatedInlineResponse201;
    }
    resource function get students/[int studentNumber]() returns Student|http:NotFound {

    }
    resource function put students/[int studentNumber](@http:Payload Student payload) returns typedesc<http:Unauthorized> {
        typedesc<http:Unauthorized> typedescResponse = http:Unauthorized;
        return typedescResponse;
    }
    resource function delete students/[int studentNumber]() returns typedesc<http:RequestMessage> {
        typedesc<http:RequestMessage> typedescResult = http:RequestMessage;
        return typedescResult;
    }
}
