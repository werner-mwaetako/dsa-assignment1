import ballerina/http;

public type NotFoundString record {|
    *http:NotFound;
    string body;
|};

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type CourseDetails record {
    string course_code;
    float assesment_weight;
    float assesment_marks;
};

public type InlineResponse201 record {
    # the username of the student newly created
    string userid?;
};

public type Student record {
    # Student number uniquely identifies a student
    int student_number;
    string student_name;
    string email_address;
    CourseDetails enrolled_courses;
};
