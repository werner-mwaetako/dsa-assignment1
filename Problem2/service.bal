
import ballerina/graphql;

public type CovidCase record {|
    string date;
    readonly string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
|};

table<CovidCase> key(region) CovidDb = table [
        {date: "2020-09-13", region: "Khomas", deaths: 28, confirmed_cases: 34, recoveries: 8, tested: 3},
        {date: "2020-09-13", region: "Erongo", deaths: 19, confirmed_cases: 50, recoveries: 4, tested: 16},
        {date: "2020-09-15", region: "Kavango-East", deaths: 3, confirmed_cases: 12, recoveries: 21, tested: 6},
        {date: "2020-09-14", region: "Kavango-West", deaths: 2, confirmed_cases: 28, recoveries: 7, tested: 13},
        {date: "2020-09-12", region: "Otjozondjupa", deaths: 13, confirmed_cases: 34, recoveries: 8, tested: 24},
        {date: "2020-09-12", region: "Omaheke", deaths: 16, confirmed_cases: 68, recoveries: 19, tested: 20},
        {date: "2020-09-13", region: "Oshana", deaths: 25, confirmed_cases: 76, recoveries: 20, tested: 43},
        {date: "2020-09-12", region: "Ohangwena", deaths: 14, confirmed_cases: 59, recoveries: 18, tested: 3},
        {date: "2020-09-13", region: "Zambezi", deaths: 16, confirmed_cases: 53, recoveries: 48, tested: 37},
        {date: "2020-09-13", region: "Hardap", deaths: 12, confirmed_cases: 46, recoveries: 12, tested: 18},
        {date: "2020-09-15", region: "Kunene", deaths: 18, confirmed_cases: 14, recoveries: 8, tested: 3},
        {date: "2020-09-13", region: "Omusati", deaths: 12, confirmed_cases: 90, recoveries: 63, tested: 28},
        {date: "2020-09-14", region: "Oshikoto", deaths: 17, confirmed_cases: 14, recoveries: 8, tested: 45},
        {date: "2020-09-14", region: "Karas", deaths: 3, confirmed_cases: 16, recoveries: 9, tested: 47}
    ];

public distinct service class CovidInfo {
    private final readonly & CovidCase case;

    function init(CovidCase case) {
        self.case = case.cloneReadOnly();
    }

    resource function get region() returns string {
        return self.case.region;
    }

    resource function get date() returns string {
        return self.case.date;
    }

    resource function get confirmed_cases() returns decimal? {
        return self.case.confirmed_cases;
    }

    resource function get deaths() returns decimal? {
        return self.case.deaths;
    }

    resource function get recoveries() returns decimal? {
        return self.case.recoveries;
    }

    resource function get tested() returns decimal? {
        return self.case.tested;
    }
}

service /dashboard on new graphql:Listener(9000) {
    //method to get all of the covid statistics data
    resource function get all() returns CovidInfo[] {
        CovidCase[] covidCases = CovidDb.toArray().cloneReadOnly();
        return covidCases.map(entry => new CovidInfo(entry));
    }

    //filter out the records to a specific entry
    resource function get filter(string region) returns CovidInfo? {
        CovidCase? case = CovidDb[region];
        if case is CovidCase {
            return new (case);
        }
        return;
    }

    //add a graphql mutation to the covid cases
    remote function add(CovidCase case) returns CovidInfo {
        CovidDb.add(case);
        return new CovidInfo(case);
    }
}

// Run this line in the terminal to display output
//curl -X POST -H "Content-type: application/json" -H "scope: unknown" -d '{ "query": "query { all {  date region  deaths confirmed_cases recoveries tested}  }" }' 'http://localhost:9000/dashboard'

