import ballerina/io;

amsClient ep = check new ("http://localhost:9090");

public function main() returns error? {


    //Datasets

    User[] newUsers = [
        {userId:"1", firstname:"John", lastname:"Doe", email:"jdoe@gmail.com", profile:{username:"jayd", user_type:"learner", courses:[]}},
        {userId:"2", firstname:"Ben", lastname:"Exile", email:"brex@gmail.com", profile:{username:"brex", user_type:"learner", courses:[]}},
        {userId:"3", firstname:"Kina", lastname:"Meyi", email:"meyk@gmail.com", profile:{username:"meyi", user_type:"adminstrator", courses:[]}},
        {userId:"4", firstname:"Jane", lastname:"Lint", email:"jlint@gmail.com", profile:{username:"lint", user_type:"adminstrator", courses:[]}},
        {userId:"5", firstname:"Jose", lastname:"Canselo", email:"jcelo@gmail.com", profile:{username:"celo", user_type:"assessor", courses:[]}},
        {userId:"6", firstname:"Tanya", lastname:"Mvulanyengo", email:"mvula@gmail.com", profile:{username:"mvula", user_type:"assessor", courses:[]}},
        {userId:"7", firstname:"Alu", lastname:"Menu", email:"alm@gmail.com", profile:{username:"almnu", user_type:"learner", courses:[]}},
        {userId:"8", firstname:"Musa", lastname:"Musa", email:"musa@gmail.com", profile:{username:"musa", user_type:"learner", courses:[]}}
    ];

    Course[] courses = [
        {course_code:"CSC101", course_name:"Introduction to Computer Science", course_description:"This course introduces the basic concepts of computer science", course_instructor:"", assignments:[]},
        {course_code:"OPS102", course_name:"Operating Systems", course_description:"This course introduces the basic software architectures and algorithms", course_instructor:"", assignments:[]},
        {course_code:"CSC103", course_name:"Data Structures", course_description:"This course introduces the basic data structures and algorithms", course_instructor:"", assignments:[]},
        {course_code:"CSC104", course_name:"Algorithms", course_description:"This course introduces the basic algorithms and their complexity", course_instructor:"", assignments:[]},
        {course_code:"CSC105", course_name:"Computer Architecture", course_description:"This course introduces the basic computer architecture and its components", course_instructor:"", assignments:[]},
        {course_code:"CSC106", course_name:"Computer Networks", course_description:"This course introduces the basic computer networks and their protocols", course_instructor:"", assignments:[]},
        {course_code:"CSC107", course_name:"Database Systems", course_description:"This course introduces the basic database systems and their models", course_instructor:"", assignments:[]},
        {course_code:"CSC108", course_name:"Software Engineering", course_description:"This course introduces the basic software engineering concepts", course_instructor:"", assignments:[]},
        {course_code:"CSC109", course_name:"Artificial Intelligence", course_description:"This course introduces the basic concepts of artificial intelligence", course_instructor:"", assignments:[]},
        {course_code:"CSC110", course_name:"Machine Learning", course_description:"This course introduces the basic concepts of machine learning", course_instructor:"", assignments:[]},
        {course_code:"CSC111", course_name:"Computer Graphics", course_description:"This course introduces the basic concepts of computer graphics", course_instructor:"", assignments:[]},
        {course_code:"CSC112", course_name:"Computer Security", course_description:"This course introduces the basic concepts of computer security", course_instructor:"", assignments:[]},
        {course_code:"CSC113", course_name:"Computer Vision", course_description:"This course introduces the basic concepts of computer vision", course_instructor:"", assignments:[]},
        {course_code:"CSC114", course_name:"Computer Forensics", course_description:"This course introduces the basic concepts of computer forensics", course_instructor:"", assignments:[]},
        {course_code:"CSC115", course_name:"Computer Games", course_description:"This course introduces the basic concepts of computer games", course_instructor:"", assignments:[]},
        {course_code:"CSC116", course_name:"Computer Animation", course_description:"This course introduces the basic concepts of computer animation", course_instructor:"", assignments:[]},
        {course_code:"CSC117", course_name:"Computer Simulation", course_description:"This course introduces the basic concepts of computer simulation", course_instructor:"", assignments:[]}
    ];

    Assignment[] assignments = [
        {email:"jdoe@gmail.com", course_code:"CSC101", assignment_name:"Assignment 1", assignment_due_date:"2021-05-01", marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC103", assignment_name:"Assignment 2", assignment_due_date:"2021-05-02",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC104", assignment_name:"Assignment 3", assignment_due_date:"2021-05-03",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC106", assignment_name:"Assignment 5", assignment_due_date:"2021-05-05",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC107", assignment_name:"Assignment 6", assignment_due_date:"2021-05-06",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC108", assignment_name:"Assignment 7", assignment_due_date:"2021-05-07",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC109", assignment_name:"Assignment 8", assignment_due_date:"2021-05-08",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC110", assignment_name:"Assignment 9", assignment_due_date:"2021-05-09",  marks:0, weight:40, marked:false},
        {email:"", course_code:"CSC111", assignment_name:"Assignment 10", assignment_due_date:"2021-05-10",  marks:0, weight:40, marked:false},
        {email:"", course_code:"OPS102", assignment_name:"Assignment 11", assignment_due_date:"2021-05-11",  marks:0, weight:40, marked:false}
    ];

    Marks[] marks = [
        {assignment_name:"Assignment 1", assignment_marks:34, assignment_course:"CSC108", learner_email:"jdoe@gmail.com"},
        {assignment_name:"Assignment 2", assignment_marks:26, assignment_course:"CSC103", learner_email:"jdoe@gmail.com"},
        {assignment_name:"Assignment 3", assignment_marks:36, assignment_course:"CSC104", learner_email:"jdoe@gmail.com"},
        {assignment_name:"Assignment 4", assignment_marks:39, assignment_course:"CSC105", learner_email:"jdoe@gmail.com"},
        {assignment_name:"Assignment 5", assignment_marks:17, assignment_course:"CSC106", learner_email:"jdoe@gmail.com"},
        {assignment_name:"Assignment 6", assignment_marks:20, assignment_course:"CSC107", learner_email:"brex@gmail.com"}
    ];

    Register[] informations = [
        {userId:"1", course_code:"CSC101"},
        {userId:"1", course_code:"CSC103"},
        {userId:"1", course_code:"CSC104"},
        {userId:"1", course_code:"CSC105"},
        {userId:"1", course_code:"CSC106"}
    ];


    // Operations

    //Creating users to the system
    io:println("Making  a create user CLIENT streaming rpc.......");

    Create_usersStreamingClient create_usersStreamingClient = check ep->create_users();

    foreach User create_usersRequest in newUsers{
        check create_usersStreamingClient->sendUser(create_usersRequest);
    }
    check create_usersStreamingClient->complete();
    string? create_usersResponse = check create_usersStreamingClient->receiveString();
    io:println(create_usersResponse);

    //Creating courses to the system
    io:println("Making  a create courses BI-DIRECTIONAL streaming rpc.......");    
    Create_coursesStreamingClient create_coursesStreamingClient = check ep->create_courses();
    future<error?> future1 = start fetchResponse(create_coursesStreamingClient);

    foreach Course create_coursesRequest in courses{
        check create_coursesStreamingClient->sendCourse(create_coursesRequest);
    }
    check create_coursesStreamingClient->complete();
    CreateCourseResponse? create_coursesResponse = check create_coursesStreamingClient->receiveCreateCourseResponse();
    io:println(create_coursesResponse);

    error? unionResult = wait future1;
    unionResult = check unionResult;

    //Assigning courses to the assessor
    AssignCourseRequest assign_coursesRequest = {userId: "20", course_code: "CSC107"};
    string assign_coursesResponse = check ep->assign_courses(assign_coursesRequest);
    io:println(assign_coursesResponse);
    
    //For student to register to the course
    io:println("Making  a register CLIENT streaming rpc.......");
    RegisterStreamingClient registerStreamingClient = check ep->register();
    foreach Register registerRequest in informations{
            check registerStreamingClient->sendRegister(registerRequest);
    }
    check registerStreamingClient->complete();
    string? registerResponse = check registerStreamingClient->receiveString();
    io:println(registerResponse);
    
    //To submit assignments
    io:println("Making  a submit assingment CLIENT streaming rpc.......");
    Submit_assignmentsStreamingClient submit_assignmentsStreamingClient = check ep->submit_assignments();
    foreach Assignment submit_assignmentsRequest in assignments{
        check submit_assignmentsStreamingClient->sendAssignment(submit_assignmentsRequest);
    }
    check submit_assignmentsStreamingClient->complete();
    string? submit_assignmentsResponse = check submit_assignmentsStreamingClient->receiveString();
    io:println(submit_assignmentsResponse);

    //To submit marks
    io:println("Making  a submit marks CLIENT streaming rpc.......");
    Submit_marksStreamingClient submit_marksStreamingClient = check ep->submit_marks();
    foreach Marks submit_marksRequest in marks{
        check submit_marksStreamingClient->sendMarks(submit_marksRequest);
    }
    check submit_marksStreamingClient->complete();
    string? submit_marksResponse = check submit_marksStreamingClient->receiveString();
    io:println(submit_marksResponse);
    
    //To request an unmarked assignment
    io:println("Making  a request assignemnt SERVER streaming rpc.......");
    string request_assignmentsRequest = "CSC101";
    stream<SingleAssignmentResponse, error?> request_assignmentsResponse = check ep->request_assignments(request_assignmentsRequest);
    check request_assignmentsResponse.forEach(function(SingleAssignmentResponse value) {
        io:println(value);
    });

}

//Function to fetch the create courses response from the server
function fetchResponse(Create_coursesStreamingClient create_course_stream) returns error?{
   CreateCourseResponse? response = check create_course_stream->receiveCreateCourseResponse();

    while !(response is ()){
        io:println("Course recieved successfully:", response);
        response = check create_course_stream->receiveCreateCourseResponse();
    }
}

