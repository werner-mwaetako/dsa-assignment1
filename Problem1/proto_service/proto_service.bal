import ballerina/log;
import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: AMS_DESC}
service "ams" on ep {

    remote function assign_courses(AssignCourseRequest value) returns string|error {
        Course[] found_course = [];
        User[] found_user = [];
        foreach Course course in courses {
            if (course.course_code == value.course_code){
                found_course.push(course);
                foreach User user in users{
                    if (user.userId == value.userId && user.profile.user_type == "assessor"){
                        found_user.push(user);
                        user.profile.courses.push(course);
                        break;
                    }
                }
            }
        }
        if (found_course.length() == 0){
            return "Course not found";
        }
        if (found_user.length() == 0){
            return "Assessor not found";
        }
        return "Course assigned.";
    }
    remote function create_users(stream<User, grpc:Error?> clientStream) returns string|error {
        User[] latestUsers = [];
        
        check clientStream.forEach(function(User value){
            log:printInfo("Received User: " + value.profile.username + "\n");
            users.push(value);
            latestUsers.push(value);
        });
        return "Users have been created successfully";
    }
    //update to submit assignment in user
    remote function submit_assignments(stream<Assignment, grpc:Error?> clientStream) returns string|error {
        check clientStream.forEach(function(Assignment value){
            foreach User user in users{
                if (user.email == value.email && user.profile.user_type == "learner"){
                    foreach Course course in user.profile.courses {
                        if (course.course_code == value.course_code) {
                            course.assignments.push(value);
                            submitedAssignments.push(value);
                            break;
                        }
                    }
                    break;
                }
            }
            }
        );
        return "Assignments have been submitted successfully";
    }
    remote function submit_marks(stream<Marks, grpc:Error?> clientStream) returns string|error {
        check clientStream.forEach(function(Marks value){
            foreach User user in users{
                if (user.email == value.learner_email){

                    foreach Course course in user.profile.courses{
                        if (course.course_code == value.assignment_course){
                            if (course.assignments.length() > 0) {
                                foreach Assignment assignment in course.assignments {
                                    if (assignment.assignment_name == value.assignment_name && assignment.weight >= value.assignment_marks) {
                                        assignment.marks = value.assignment_marks;
                                        assignment.marked = true;
                                        markedAssignments.push(assignment);
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            }
        );
        return "Marks have been submitted successfully";
    }
    remote function register(stream<Register, grpc:Error?> clientStream) returns string|error {
        check clientStream.forEach(function(Register value){
            foreach Course course in courses {
                if (course.course_code ==  value.course_code){
                    foreach User user in users {
                        if (user.userId == value.userId && user.profile.user_type == "learner") {
                            user.profile.courses.push(course);
                            break;
                        }
                    }
                    break;
                }
            }
            
            }
        );
        return "Courses have been registered successfully";
    }
    remote function request_assignments(AmsSingleAssignmentResponseCaller caller, string value) returns error? {
        foreach Course course in courses {
            if (course.course_code == value) {
                foreach Assignment assignment in course.assignments {
                    if(assignment.marked == false){
                        checkpanic caller->sendSingleAssignmentResponse({assignment:assignment});
                        break;
                    }
                }
                break;
            }
        }
        check caller->complete();
    }
    remote function create_courses(AmsCreateCourseResponseCaller caller, stream<Course, grpc:Error?> clientStream) returns error? {
        check clientStream.forEach(function(Course value){
            log:printInfo("Received Course: " + value.course_name + "\n");
            courses.push(value);
            checkpanic caller->sendCreateCourseResponse({course_code: value.course_code});
        });
       check caller->complete();
    }
}
User[] users = [];
Course[] courses = [];
Assignment[] submitedAssignments = [];
Assignment[] markedAssignments = [];